# "Java GUI - TGM Group Chat" - Taskdescription

## Die Aufgabenstellung
Erstelle ein Gruppenchat-Programm auf Basis Ihrer Implementierung von **Java GUI & Socket Programmierung - Simple Chat**. Die Implementierung muss mithilfe von Design-Patterns umgesetzt werden.

### Grundanforderungen
* GUI's mittels JavaFX erstellen
* Trennung jeweils von View und Controller sowie der Kommunikationsimplementierung
* Javadoc-Dokumentation
* Kenntnis über Design Patterns (Observer, Strategy, Command, etc.)

#### Chatfunktionalität
Basierend auf dem **Simple Chat** sollen Teilnehmer nun privat kommunizieren können. Mit dem Befehl *!PRIVATE {Franz} Hallo Franz, ich habe dir ganz viel Geld überwiesen!* soll eine private Nachricht an einen bestimmten Benutzer verschickt werden. Wenn mehrere Benutzernamen in die Liste eingefügt werden, kann eine einfache Nutzerliste addressiert werden.

Um aber eine erweiterte Gruppenadministration zu ermöglichen, ist der Befehl *!GROUP create PrivateGruppe* zu implementieren. Dabei wird ein neuer Reiter erstellt, wo der Client dann eine "Server-Oberfläche" als Admin dieser Gruppe erhält. Dort kann er neue Mitglieder einladen, entfernen und den Gruppenchat wieder löschen. Dafür sind einerseits folgende Befehle bzw. Elemente der grafischen Oberfläche zu nutzen:

* *!GROUP invite {Franz, Sissi, DarthVader}*
* *!GROUP kick {Sissi, DarthVader}*
* *!GROUP destroy*
* *!GROUP accept PrivateGrupp*
* *!GROUP leave*
* *!GROUP admin Franz*

Ein eingeladener Benutzer bekommt eine Benachrichtigung, die er entsprechend annehmen kann, oder auch nicht. Einladungen haben nur eine beschränkte Zeitdauer (z.B. 60 Sekunden). Benutzer können sich im Gruppenmodus entweder mit dem Button *Leave* oder dem Befehl in der Konsole wieder abmelden. Der Ersteller kann weitere Administratoren ernennen und nur dann den Gruppenchat verlassen, wenn er nicht der letzte Admin war. Der letzte Admin kann den Gruppenchat nur verlassen, wenn er die Gruppe zerstört. Der Administrator kann auch entsprechende Benutzer wieder aus dem Chat entfernen.

Als erweiterte Funktionalität soll auch in der Gruppe nur eine bestimmte Menge an Nutzer angeschrieben werden, die entweder per Privat-Nachricht per Eingabe oder Auswahl in der grafischen Oberfläche stattfinden soll.

#### Server
Der Server soll die Verwaltung der Gruppenchats mittels Observer-Pattern implementieren. Dabei ist auch der Erweiterung der Kommandos Rechnung zu tragen (z.B. mittels Command-Pattern).

Die TCP-Konnektivität soll durch andere Protokolle implementiert werden. Es sollen dabei alle Funktionalitäten beibehalten werden. Anbieten dafür wäre eine Implementierung über eine Message-Oriented-Middleware. Der bestehende Code soll dabei so wenig wie möglich angepasst und die Interfaces nur mit Absprache des Auftraggebers geändert werden.

### Erweiterungen
Der User kann den Chatnamen bei Programmaufruf mitgeben:

    java tgmchat.client.TgmChat --name Franz --host localhost --port 5050

Bei Aufruf mit Gradle sind die Argumente in folgender Weise zu übergeben:

    gradle server --args="--host localhost --port 5050 -v"
    gradle client --args="--name Franz --host localhost --port 5050"

Halte deine Erkenntnisse im [Readme](README.md) fest und dokumentiere deine Lösung!

### Umgebung und Tests
Es wird Gradle als Build-Umgebung verwendet. Um eine Liste an Tasks zu erhalten können folgende Befehle ausgeführt werden:

    gradle tasks

Um die Umgebung auch für IntelliJ oder eclipse einzurichten, kann der folgende Task ausgeführt werden:

    gradle idea eclipse

Es ist notwendig eigene Tests zur Funktionsüberprüfung zu schreiben.
Hierzu sind einige Testbeispiele schon vorhanden.

Folgender Befehl wird bei der Abnahme ausgeführt und bewertet:

    gradle clean test jacocoTestReport javadoc

## Bewertung
### EK Anforderungen überwiegend erfüllt
- [ ] Private Nachrichten an einen oder mehrere Benutzer versendbar
- [ ] Gruppenchat-Erstellung und Löschung möglich
- [ ] Gruppenadministrator implementiert
- [ ] Mitgliederverwaltung implementiert
- [ ] Software-Testing mit Testreport

### EK Anforderungen zur Gänze erfüllt
- [ ] private Nachricht in Gruppenchats
- [ ] MoM Implementierung
- [ ] Ausführliches Software-Testing
- [ ] Coverage Report


## Quellen
\[1] JavaFX Manual <https://docs.oracle.com/javase/8/javase-clienttechnologies.htm>  
\[2] Java Sockets Tutorial <https://docs.oracle.com/javase/tutorial/networking/sockets/index.html>
\[3] Tips for Sizing and Aligning Nodes <https://docs.oracle.com/javafx/2/layout/size_align.htm>
