package simplechat.gui;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit.ApplicationTest;
import simplechat.client.Controller;
import simplechat.client.FXApplication;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.matcher.base.NodeMatchers.hasChildren;

/**
 * Checks if the graphical interface of the server has all components
 * @author Kacper Urbaniec
 * @version 24.01.2019
 */
public class ServerGUITest extends ApplicationTest {
    private Controller controller;
    private TextArea textArea;
    private Parent mainNode;

    @Override
    public void start(Stage stage) throws Exception {
        mainNode = FXMLLoader.load(FXApplication.class.getResource("/server.fxml"));
        stage.setScene(new Scene(mainNode, 300, 275));
        stage.show();
        stage.toFront();
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
        FxToolkit.hideStage();
        release(new KeyCode[]{});
        release(new MouseButton[]{});
    }

    @Test
    public void testHasButtonSend() {
        Set<Button> buttons = from(mainNode).lookup(".button").queryAll();
        for(Button button: buttons) {
            if (button.getText().equals("Send")) {
                return;
            }
        }
        fail();
    }

    @Test
    public void testHasButtonRemove() {
        Set<Button> buttons = from(mainNode).lookup(".button").queryAll();
        for(Button button: buttons) {
            if (button.getText().equals("Remove")) {
                return;
            }
        }
        fail();
    }

    @Test
    public void testHasTextArea() {
        verifyThat("#grid", hasChildren(1, "#textArea"));
    }

    @Test
    public void testHasTextField() {
        verifyThat("#grid", hasChildren(1, "#textField"));
    }

    @Test
    public void testTextFieldText() {
        TextArea area = from(mainNode).lookup("#textArea").query();
        String text = area.getText();
        assertEquals("Welcome to Simple Chat!", area.getText());
    }

    @Test
    public void testHasTextListView() {
        verifyThat("#grid", hasChildren(1, "#listView"));
    }
}
