package tgmchat;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.util.Random;

/**
 * @author Kacper Urbaniec
 * @version 2019-06-10
 * Class for testing purposes
 */
public class JMSTest {

    //URL of the JMS server. DEFAULT_BROKER_URL will just mean that JMS server is on localhost
    private static String url = ActiveMQConnection.DEFAULT_BROKER_URL;

    // default broker URL is : tcp://localhost:61616"
    private static String subject = "server-registration"; // Queue Name.You can create any/many queue names as per your requirement.

    public static void main(String[] args) throws JMSException {
        // Getting JMS connection from the server and starting it
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
        Connection connection = connectionFactory.createConnection();
        connection.start();

        //Creating a non transactional session to send/receive JMS message.
        Session session = connection.createSession(false,
                Session.AUTO_ACKNOWLEDGE);

        //Destination represents here our queue 'JCG_QUEUE' on the JMS server.
        //The queue will be created automatically on the server.
        // GroupChat Server QUeue
        Destination destination = session.createQueue(subject);

        // MessageProducer is used for sending messages to the queue.
        MessageProducer producer = session.createProducer(destination);

        Destination replyDest = session.createTemporaryQueue();

        // We will send a small text message saying 'Hello World!!!'
        TextMessage message = session
                .createTextMessage("Hello !!! Welcome to the world of ActiveMQ.");

        message.setJMSReplyTo(replyDest);
        message.setJMSCorrelationID(Long.toHexString(new Random(System.currentTimeMillis()).nextLong()));


        // Here we are sending our message!
        producer.send(message);

        System.out.println("JCG printing@@ '" + message.getText() + "'");

        MessageConsumer consumer = session.createConsumer(replyDest);
        TextMessage reply = (TextMessage)consumer.receive();
        System.out.println("RECEIVED: "+reply.getText());

        session.close();
        connection.close();
    }

}
