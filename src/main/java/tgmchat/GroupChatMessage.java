package tgmchat;

/**
 * Object that represents a message in the group-chat application.
 * @author Kacper Urbaniec
 * @version 2019-06-10
 */
public class GroupChatMessage {
    private String sender;
    private String message;

    public GroupChatMessage(String sender, String message) {
        this.sender = sender;
        this.message = message;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
