package simplechat.client;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Controller {

    private SimpleChat simpleChat;

    private ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);

    @FXML
    private TextField textField;

    @FXML
    private TextArea textArea;

    @FXML
    private Text actionTarget = null;

    // Store client information for groups
    @FXML
    private ListView listView;
    private HashMap<String, String[]> chatClients;

    @FXML
    private VBox chatPanel;

    // Store chat history
    private HashMap<String, String> chatHistory;
    private String activeChat = "Alle";
    private boolean input = false;

    public void initController() {
        chatHistory = new HashMap<>();
        chatHistory.put("Alle", "Welcome to Group Chat");
        chatClients = new HashMap<>();
    }

    @FXML
    protected void handleMessageButtonAction(ActionEvent event) {
        event.consume();
        sendMessage();
    }

    public void initialize() {
        textField.setOnKeyReleased(event -> {
            if (event.getCode() == KeyCode.ENTER){
                sendMessage();
            }
        });
        Platform.runLater(() -> textField.requestFocus());
    }

    public void stop() {
        simpleChat.stop();
        scheduledExecutorService.shutdownNow();
    }

    public void setSimpleChat(SimpleChat simpleChat) {
        this.simpleChat = simpleChat;
    }

    public void updateTextAreaWithText(String text) {
        chatHistory.put("Alle", chatHistory.get("Alle") + "\n" + text);
        if(activeChat.equals("Alle")) {
            textArea.setText(chatHistory.get("Alle"));
            textArea.appendText("");
        }
    }

    public void updateTextAreaWithText(String text, String groupName) {
        chatHistory.put(groupName, chatHistory.get(groupName) + "\n" + text);

        if(activeChat.equals(groupName)) {
            textArea.setText(chatHistory.get(groupName));
            textArea.appendText("");
        }
    }

    public void sendMessage() {
        String message = textField.getText();
        if (!message.isEmpty()) {
            if(!activeChat.equals("Alle"))
                simpleChat.sendMessage(message, activeChat);
            else simpleChat.sendMessage(message);
        }
        if (simpleChat.isConnected()) {
            textField.clear();
            actionTarget.setText("Message sent.");
            scheduledExecutorService.schedule(clearText, 1, TimeUnit.SECONDS);
        }
    }

    @FXML
    protected void changeChat(ActionEvent event) {
        String groupName = ((Button)event.getSource()).getText();
        if (chatHistory.containsKey(groupName)) {
            activeChat = groupName;
            String history = chatHistory.get(groupName);
            textArea.setText(history);
            textArea.appendText("");
            Platform.runLater(() -> {
                listView.getItems().clear();
                try {
                    listView.getItems().addAll(chatClients.get(groupName));
                }
                catch (Exception ex) { }
            });
        }
    }

    public void setClients(String groupName, String clients) {
        chatClients.put(groupName, clients.split(" "));
        Platform.runLater(() -> {
            listView.getItems().clear();
            try {
                if(activeChat != "Alle")
                    listView.getItems().addAll(chatClients.get(groupName));
            }
            catch (Exception ex) { }
        });
    }

    public void addChat(String groupName) {
        if(!chatHistory.containsKey(groupName)) {
            chatHistory.put(groupName, "Welcome to Group Chat");
            Button newGroup = new Button(groupName);
            newGroup.setOnAction(this::changeChat);
            newGroup.setMaxWidth(Double.MAX_VALUE);
            //newGroup.setBackground(new Background(new BackgroundFill(Color.web("#2196f3"), CornerRadii.EMPTY, Insets.EMPTY)));
            Platform.runLater(() -> chatPanel.getChildren().add(newGroup));
        }
        activeChat = groupName;
    }

    public void removeChat(String groupName) {
        activeChat = "Alle";
        chatHistory.remove(groupName);
        List<Node> buttons = chatPanel.getChildren();
        for(Node button: buttons) {
            if(button instanceof Button) {
                String chatName = ((Button) button).getText();
                if (chatName.equals(groupName)) {
                    Platform.runLater(() -> chatPanel.getChildren().remove(button));
                    break;
                }
            }
        }
        updateTextAreaWithText("");
        textArea.setText(chatHistory.get("Alle"));
        textArea.appendText("");
        Platform.runLater(() -> {
            listView.getItems().clear();
            try {
                listView.getItems().clear();
            }
            catch (Exception ex) { }
        });
    }

    public boolean showInviteDialog(String groupName) {
        final CountDownLatch latch = new CountDownLatch(1);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Group-Dialog");
                alert.setHeaderText("Do you want to join " + groupName);
                alert.setContentText("Are you ok with this?");

                Thread thread = new Thread(() -> {
                    try {
                        // Wait for 5 secs
                        Thread.sleep(10000);
                        if (alert.isShowing()) {
                            Platform.runLater(() -> alert.close());
                        }
                    } catch (Exception exp) {
                        exp.printStackTrace();
                    }
                });
                thread.start();

                try {
                    Optional<ButtonType> result = alert.showAndWait();
                    if (result.get() == ButtonType.OK) {
                        input = true;
                    }
                    else input = false;
                }
                catch(Exception ex) {
                    input = false;
                }
                latch.countDown();
            }
        });
        try {
            latch.await();
        }
        catch (Exception ex) {
            input = false;
        }
        return input;
    }

    Runnable clearText = () -> {
        actionTarget.setText("");
    };
}
