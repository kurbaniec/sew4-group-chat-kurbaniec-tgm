package simplechat.server;

import simplechat.communication.socket.server.SimpleChatServer;

import org.apache.commons.cli.*;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.logging.Logger;
import java.util.logging.ConsoleHandler;

import static java.util.logging.Level.*;

/**
 * The Server Class choosing the communication framework and user interface
 *
 * @author Michael Borko  {@literal <mborko@tgm.ac.at>}
 * @version 1.0
 */
public class SimpleChat {

    private SimpleChatServer server;
    private ConcurrentSkipListSet<String> users;

    private ConcurrentLinkedQueue<String> receivedMessages;
    private ConcurrentLinkedQueue<String> sentMessages;
    private HashMap<String, ArrayList<String>> groups = new HashMap<>();

    private Controller controller;

    public static Logger serverLogger = Logger.getLogger("server");

    /**
     * Definition of Server Information
     * <br>
     * There are three optional arguments, which can be parsed through the
     * <a href="https://commons.apache.org/proper/commons-cli/javadocs/api-release/index.html">
     * Apache CommonsCLI Library</a>.
     *
     * @param args <br>
     *             Server hostname, e.g. --host 10.0.15.3 or -h 10.0.15.3 <br>
     *             TCP port to listen on, e.g. --port 1234 or -p 1234 <br>
     *             explaining what is being done, e.g. --verbose or -v <br>
     */
    public static void main(String[] args) {

        serverLogger.setLevel(FINE);
        serverLogger.setUseParentHandlers(false);
        ConsoleHandler ch = new ConsoleHandler();
        ch.setLevel(SEVERE);
        serverLogger.addHandler(ch);
        
        CommandLineParser parser = new DefaultParser();
        Options options = new Options();
        options.addOption("h", "host", true, "Server hostname.");
        options.addOption("p", "port", true, "TCP port to listen.");
        options.addOption("v", "verbose", false, "explain what is being done");

        CommandLine line;
        String host = null;
        Integer port = null;
        try {
            line = parser.parse(options, args);
            host = line.getOptionValue("h");
            port = line.getOptionValue("p") != null ? Integer.parseInt(line.getOptionValue("p")) : null;

            boolean verbose = line.hasOption("v");
            if (verbose) ch.setLevel(ALL);

            serverLogger.log(INFO, "Parameters set by user: " +
                    "host=" + host + " port=" + port + " verbose=" + verbose);
        } catch (ParseException e) {
            serverLogger.log(SEVERE, e.toString());
            System.exit(1);
            // TODO UI ErrorMessage!
        }

        SimpleChat simpleChat = new SimpleChat(host, port);
        simpleChat.listen();

        FXApplication fxApplication = new FXApplication();
        fxApplication.setSimpleChat(simpleChat);
        fxApplication.main(args);
    }

    /**
     * Initiating server Thread and the user list {@link #users}.
     *
     * @param host hostname definition for server Thread
     * @param port port on which server Thread should listen
     */
    public SimpleChat(String host, Integer port) {

        server = new simplechat.communication.socket.server.SimpleChatServer(host, port, this);
        users = new ConcurrentSkipListSet<>();
        receivedMessages = new ConcurrentLinkedQueue<>();
        sentMessages = new ConcurrentLinkedQueue<>();
    }

    /**
     * @param controller UI Controller for message and configuration interaction
     */
    public void setController(Controller controller) {
        this.controller = controller;
    }

    /**
     * After successfully initiating the server Thread, here the concurrent execution will be started.
     */
    public void listen() {
        serverLogger.log(INFO, "Initiating SimpleChatServer ...");
        server.start();
    }

    /**
     * Gracefully shutdown of server Thread calling {@link SimpleChatServer#shutdown()}
     */
    public void stop() {
        //if (isConnected())
        server.shutdown();
    }

    /**
     * @return checks if server Thread is still alive
     */
    public boolean isConnected() {
        return server.isAlive();
    }

    /**
     * Passing message to networkHandler, only if the Thread is still alive
     *
     * @param message plain message
     */
    public void sendMessage(String message) {
        serverLogger.log(INFO, "UI gave me this message: " + message);
        if (isConnected()) {
            server.send(message);
            sentMessages.add(message);
        }
    }

    /**
     * Group messages sending handler.
     *
     * @param message  plain message
     * @param groupName receiver-group name
     */
    public void sendMessage(String message, String groupName) {
        serverLogger.log(INFO, "UI gave me this message: " + message + " for this user: " + groupName);
        //if (isConnected()) {
        server.send(message, groupName);
        sentMessages.add(groupName + " -> " + message);
        //}
    }

    /**
     * Got a new message from communication framework
     *
     * @param message Message sent by Client
     */
    public void incomingMessage(String message) {
        sendMessage(message);
        receivedMessages.add(message);
        if (controller != null) controller.updateTextAreaWithText(message);
    }

    // TODO incomingGroupMessage
    public void incomingMessage(String message, String groupName) {
        sendMessage(message, groupName);
        //receivedMessages.add(message);
        //if (controller != null) controller.updateTextAreaWithText(message);
    }



    /**
     * Returns list of connected Users
     *
     * @return Array of unique chatNames of connected Clients
     */
    public synchronized String[] getClients() {
        return users.toArray(new String[0]);
    }

    public synchronized ArrayList<String> getGroup(String groupName) {
        return groups.get(groupName);
    }
    /**
     * Adds a Client to the userList. The check of the username must be synchronized!
     *
     * @param chatName Client which will be added
     * @return New unique ChatName. If the given Name was unique the same as the {@code chatName}
     * or an adapted new name (e.g. Franz#1)
     */
    public synchronized String addClient(String chatName) {
        String name = chatName.equals("") ? "Client" : chatName;
        while (users.contains(name)) {
            if (!name.contains("#")) {
                name += "#1";
            } else {
                int i = Integer.parseInt(name.substring(name.lastIndexOf("#") + 1));
                name = name.substring(0, name.lastIndexOf("#") + 1) + (i + 1);
            }
        }
        users.add(name);
        if (controller != null) controller.addUser(name);
        return name;
    }

    /**
     * Renames Client in local userlist {@link #users} by removing the oldChatName and inserting the newChatName
     *
     * @param oldChatName Clientname which will be removed from list
     * @param newChatName Clientname which should be added by {@link simplechat.server.SimpleChat#addClient(String)}
     * @return New unique ChatName. If the given Name was unique the same as the {@code newChatName}
     * or an adapted new name (e.g. Franz#1)
     */
    public synchronized String renameClient(String oldChatName, String newChatName) {
        removeClient(oldChatName);
        newChatName = addClient(newChatName);
        renameUserInGroup(oldChatName, newChatName);
        return newChatName;
    }

    /**
     * If chatName exists in userlist {@link #users}, user will be informed of removal.
     * Afterwards Client will be removed from userlist
     * and also the UserInterface method {@link Controller#removeUser(String)} will be called.
     *
     * @param chatName Client which will be removed from Userlist
     */
    public void removeClient(String chatName) {
        if (users.contains(chatName)) {
            users.remove(chatName);
            if (controller != null) controller.removeUser(chatName);
        }
    }

    /**
     * Calls {@link simplechat.communication.socket.server.SimpleChatServer#removeClient(String)} to shutdown client
     * and remove it internally by calling {@link #removeClient(String)}.
     *
     * @param chatName Client which will be informed of shutdown
     */
    public void shutdownClient(String chatName) {
        if (server.isAlive()) server.removeClient(chatName);
        removeClient(chatName);
    }

    public String addGroup(String chatName, String admin) {
        String name = chatName.equals("") ? "Group" : chatName;
        for(String searcher : groups.keySet()) {
            if(searcher.equals(name)) {
                if (!name.contains("#")) {
                    name += "#1";
                } else {
                    int i = Integer.parseInt(name.substring(name.lastIndexOf("#") + 1));
                    name = name.substring(0, name.lastIndexOf("#") + 1) + (i + 1);
                }
            }
        }
        ArrayList<String> group = new ArrayList<>();
        group.add(admin);
        group.add(admin);

        groups.put(name, group);
        //if (controller != null) controller.addUser(name);
        return name;
    }

    public void addUsersToGroup(String groupName, String[] users) {
        ArrayList<String> group = groups.get(groupName);
        group.addAll(Arrays.asList(users));
    }

    public void addUserToGroup(String groupName, String user) {
        ArrayList<String> group = groups.get(groupName);
        group.add(user);
    }

    public boolean kickUsersFromGroup(String groupName, String[] users, String admin) {
        ArrayList<String> group = groups.get(groupName);
        // Check if user that wants to kick is admin
        if(group.get(0).contains(admin)) {
            // Can´t kick oneself
            if (!Arrays.asList(users).contains(admin)) {
                group.removeAll(Arrays.asList(users));
                return true;
            }
        }
        return false;
    }

    public void renameUserInGroup(String oldChatName, String newChatName) {
        for(String key: groups.keySet()) {
            ArrayList<String> group = groups.get(key);
            for(int i = 0; i < group.size(); i++) {
                if(group.get(i).contains(oldChatName)) {
                    group.set(i, group.get(i).replace(oldChatName, newChatName));
                }
            }
            //sendMessage("user " + getGroupClients(key), key);
        }
    }

    public void renameSend(String newChatName) {
        for(String key: groups.keySet()) {
            ArrayList<String> group = groups.get(key);
            if(group.contains(newChatName)) {
                sendMessage("user " + getGroupClients(key), key);
            }
        }
    }

    public boolean addAdminToGroup(String groupName, String newAdmin, String admin) {
        ArrayList<String> group = groups.get(groupName);
        // Check if user that wants to add a new admin is admin
        if(group.get(0).contains(admin)) {
            group.set(0, group.get(0) + " " + newAdmin);
            return true;
        }
        return false;
    }

    public boolean leaveGroup(String groupName, String leaver) {
        ArrayList<String> group = groups.get(groupName);
        if(!group.get(0).contains(leaver)) {
            group.remove(leaver);
            return true;
        }
        else {
            String admins = group.get(0);
            if(!admins.replace(leaver, "").replace(" ", "").equals("")) {
                group.remove(leaver);
                group.set(0, group.get(0).replace(leaver, ""));
                return true;
            }
        }
        return false;
    }

    public void destroyGroup(String groupName, String admin) {
        ArrayList<String> group = groups.get(groupName);
        if(group.size() == 2) {
            String admins = group.get(0);
            if(admins.replace(admin, "").replace(" ", "").equals("")) {
                sendMessage("kick", groupName);
            }
        }
    }

    public String getGroupClients(String groupName) {
        ArrayList<String> group = groups.get(groupName);
        StringBuilder sb = new StringBuilder();
        for(int i = 1; i < group.size(); i++) {
            sb.append(group.get(i));
            if(group.get(0).contains(group.get(i))) {
                sb.append("[Admin]");
            }
            sb.append(" ");
        }
        return sb.toString();
    }


    /**
     * @return Queue of current received messages.
     */
    public Queue<String> getReceivedMessages() {
        return new LinkedList<>(receivedMessages);
    }

    /**
     * @return Queue of current sent messages.
     */
    public Queue<String> getSentMessages() {
        return new LinkedList<>(sentMessages);
    }

    public HashMap<String, ArrayList<String>> getGroups() {
        return groups;
    }
}
