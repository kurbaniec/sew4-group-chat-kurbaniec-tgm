package simplechat.communication.socket.server;

import com.sun.security.ntlm.Client;

import org.apache.activemq.benchmark.Consumer;
import org.apache.activemq.benchmark.Producer;
import simplechat.communication.MessageProtocol;
import simplechat.server.SimpleChat;

import static java.util.logging.Level.*;
import static simplechat.communication.MessageProtocol.Commands.EXIT;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.jms.*;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import sun.security.krb5.internal.crypto.Des;


/**
 * SimpleChatServer listens to incoming SimpleChatClients with the choosen communication protocol and initiates a UI.
 * <br>
 * Default settings for the main attributes will be: host="localhost" port=5050 and backlog=5
 */
public class SimpleChatServer extends Thread {

    private Integer port = 61616;
    private String host = "localhost";
    private final Integer backlog = 5;
    private ServerSocket serverSocket = null;

    private boolean listening = false;
    private SimpleChat server;

    private Session session;
    private Connection connection;
    private MessageConsumer registrationConsumer;

    private ConcurrentHashMap<ClientWorker, String> workerList = new ConcurrentHashMap<>();
    private ExecutorService executorService = Executors.newCachedThreadPool();

    private TreeSet<Integer> freeIndex = new TreeSet<>();
    private int lastIndex = 0;


    /**
     * Initializes host, port and callback for UserInterface interactions.
     *
     * @param host   String representation of hostname, on which the server should listen
     * @param port   Integer for the listening port
     * @param server UserInterface callback reference for user interactions
     */
    public SimpleChatServer(String host, Integer port, SimpleChat server) {
        if (host != null) this.host = host;
        if (port != null) this.port = port;
        this.server = server;
        SimpleChat.serverLogger.log(INFO, "Init: host=" + this.host + " port=" + this.port);
    }

    /**
     * Initiating the ServerSocket with already defined Parameters and starts accepting incoming
     * requests. If client connects to the ServerSocket a new ClientWorker will be created and passed
     * to the ExecutorService for immediate concurrent action.
     */
    public void run() {
        SimpleChat.serverLogger.log(INFO, "... starting Thread ...");
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://" + host + ":" + port);
        connection = null;
        try {
            connection = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(false,
                    Session.AUTO_ACKNOWLEDGE);
            // Create Queues
            Destination destination = session.createQueue("server");
            MessageConsumer consumer = session.createConsumer(destination);

            Destination registration = session.createQueue("server-registration");
            registrationConsumer = session.createConsumer(registration);

        }
        catch (Exception ex) {
            SimpleChat.serverLogger.warning("Error with ApacheMQ:\n" + ex.getMessage());
        }


        listening = true;
        SimpleChat.serverLogger.log(INFO, "ServerSocket is initialized!");

        // Finding first free Client-ID

        while (listening) {
            try {
                Message message = registrationConsumer.receive();

                int index = lastIndex;
                /**
                if (!freeIndex.isEmpty()) {
                    index = freeIndex.pollFirst();
                }*/
                //else lastIndex++;
                lastIndex++;

                // Create or use new queue for cleint
                session.createQueue("Client#" + index);
                Destination workerDest = session.createQueue("Server#" + index);
                TextMessage reply = session
                        .createTextMessage("" + index);
                //System.out.println(message.getJMSDestination());
                //System.out.println(message.getJMSCorrelationID());
                reply.setJMSDestination(message.getJMSDestination());
                reply.setJMSCorrelationID(message.getJMSCorrelationID());

                MessageProducer producer = session.createProducer(message.getJMSReplyTo());
                producer.send(reply);

                Destination destination = session.createQueue("Client#" + index);
                MessageProducer workerProducer = session.createProducer(destination);
                MessageConsumer workerConsumer = session.createConsumer(workerDest);
                ClientWorker worker = new ClientWorker(index, workerProducer, workerConsumer, session, connection, this);
                if (listening) {
                    String chatName = ((TextMessage)message).getText();
                    if (!chatName.equals("")) chatName = "Client#" + workerList.size();
                    workerList.put(worker, server.addClient(chatName));
                    executorService.execute(worker);
                }

            } catch (Exception ex) {
                SimpleChat.serverLogger.log(SEVERE, ex.toString());
            }

            /**
            ClientWorker worker = new ClientWorker(serverSocket.accept(), this);
            if (listening) {
                workerList.put(worker, server.addClient(""));
                executorService.execute(worker);
            }*/
        }

    }

    /**
     * Callback method for client worker to inform server of new message arrival
     *
     * @param plainMessage MessageText sent to server without Client information
     * @param sender       {@link ClientWorker} which received the message
     */
    public void received(String plainMessage, ClientWorker sender) {
        String message = MessageProtocol.textMessage(plainMessage, workerList.get(sender));
        server.incomingMessage(message);
    }


    public void receivedGroup(String plainMessage, ClientWorker sender, String  groupName) {
        String message = MessageProtocol.textMessage(plainMessage, workerList.get(sender));
        server.incomingMessage(message, groupName);
    }

    /**
     * Sending messages to clients through communication framework
     *
     * @param message MessageText with sender ChatName
     */
    public void send(String message) {
        workerList.forEach((k, v) -> k.send(message));
    }

    /**
     * Sending messages to a group.
     * Resembles kind of the observer pattern.
     *
     * @param message  MessageText with sender ChatName
     * @param groupName target group
     */
     public void send(String message, String groupName) {
         try {
             ArrayList<String> group = server.getGroup(groupName);
             for(int i = 1; i < group.size(); i++) {
                 getWorker((Object)group.get(i)).send(message, groupName);
             }
         } catch (Exception e) {
            SimpleChat.serverLogger.log(SEVERE, e.toString());
         }
     }

    /**
     * Send to specific users from a group.
     * @param message
     * @param groupName
     * @param users
     */
    public void send(String message, String groupName, String[] users) {
        try {
            ArrayList<String> group = server.getGroup(groupName);
            for(String user: users) {
                getWorker((Object)user).send(message, groupName);
            }
        } catch (Exception e) {
            SimpleChat.serverLogger.log(SEVERE, e.toString());
        }
    }



    private ClientWorker getWorker(Object chatName) {
        return workerList.entrySet()
                .stream()
                .filter(entry -> chatName.equals(entry.getValue()))
                .map(ConcurrentHashMap.Entry::getKey)
                .findFirst().get();
    }

    /**
     * ClientWorker has the possibility to change the ChatName. This method asks the UI
     * to rename the Client and stores the returned Name in the ClientWorker-Collection
     *
     * @param chatName new Name of Client
     * @param worker   ClientWorker Thread which was initiating the renaming
     */
    void setName(String chatName, ClientWorker worker) {
        // server.renameClient checks if name is unique and returns the name which was set
        // send("User <" + workerList.get(worker) + "> changed name to <" + chatName + ">");
        chatName = server.renameClient(workerList.get(worker), chatName);
        workerList.replace(worker, chatName);
        server.renameSend(chatName);
    }

    /**
     * Remove only this worker from the list,
     * shutdown the ClientWorker and also inform gui about removal.
     *
     * @param worker ClientWorker which should be removed
     */
    void removeClient(ClientWorker worker) {
        if (workerList.containsKey(worker)) {
            server.removeClient(workerList.get(worker));
            freeIndex.add(worker.getId());
            worker.shutdown();
            workerList.remove(worker);
        }
    }

    /**
     * Gets the ClientWorker of the given chatName and calls the private Method {@link #removeClient(String)}
     * This method will remove the worker from the list shutdown the ClientWorker and also inform gui about removal.
     *
     * @param chatName Client name which should be removed
     */
    public void removeClient(String chatName) {
        removeClient(getWorker(chatName));
        freeIndex.add(getWorker(chatName).getId());
    }

    /**
     * Add group to system
     * @param chatName
     * @param worker
     */
    public String addGroup(String chatName, ClientWorker worker) {
        String admin = workerList.get(worker);
        return server.addGroup(chatName, admin);
    }

    public void addUsersToGroup(String groupName, String[] users) {
        server.addUsersToGroup(groupName, users);
    }

    public void addUserToGroup(String groupName, String user) {
        server.addUserToGroup(groupName, user);
    }

    public boolean kickUsersFromGroup(String groupName, String[] users, ClientWorker worker) {
        String admin = workerList.get(worker);
        return server.kickUsersFromGroup(groupName, users, admin);
    }

    public boolean addAdminToGroup(String groupName, String newAdmin, ClientWorker worker) {
        String admin = workerList.get(worker);
        return server.addAdminToGroup(groupName, newAdmin, admin);
    }

    public boolean leaveGroup(String groupName, ClientWorker worker) {
        String leaver = workerList.get(worker);
        return server.leaveGroup(groupName, leaver);
    }

    public void destroyGroup(String groupName, ClientWorker worker) {
        String admin = workerList.get(worker);
        server.destroyGroup(groupName, admin);
    }

    public String getChatName(ClientWorker worker) {
        return workerList.get(worker);
    }

    public String getGroupClients(String groupName) {
        return server.getGroupClients(groupName);
    }
    /**
     * Clean shutdown of all connected Clients.<br>
     * ExecutorService will stop accepting new Thread inits.
     * After notifying all clients, ServerSocket will be closed and ExecutorService will try to shutdown all
     * active ClientWorker Threads.
     */
    public void shutdown() {
        SimpleChat.serverLogger.log(INFO, "Initiating shutdown ... ");
        listening = false;

        try  {
            workerList.forEach((k, v) -> k.shutdown());
            //serverSocket.close();
            executorService.shutdown();
            session.close();
            connection.close();
        } catch (Exception e) {
            SimpleChat.serverLogger.log(SEVERE, e.toString());
        } finally {
            executorService.shutdownNow();
            this.interrupt();
        }
        /**
        if (!serverSocket.isClosed())
            try (Socket shutdownSocket = new Socket(host, port)) {
                shutdownSocket.close();
                workerList.forEach((k, v) -> k.shutdown());
                serverSocket.close();
                executorService.shutdown();
            } catch (Exception e) {
                SimpleChat.serverLogger.log(SEVERE, e.toString());
            } finally {
                executorService.shutdownNow();
                this.interrupt();
            }
         */
    }
}

/**
 * Thread for client socket connection.<br>
 * Every client has to be handled by an own Thread.
 */
class ClientWorker implements Runnable {
    private Socket client;
    private PrintWriter out;
    private BufferedReader in;

    private SimpleChatServer callback;
    private boolean listening = true;
    //---
    private int id;
    private MessageProducer producer;
    private MessageConsumer consumer;
    private Session session;
    private Connection connection;


    /**
     * Init of ClientWorker-Thread for socket intercommunication
     *
     * @param client   Socket got from ServerSocket.accept()
     * @param callback {@link simplechat.communication.socket.server.SimpleChatServer} reference
     * @throws IOException will be throwed if the init of Input- or OutputStream fails
     */
    ClientWorker(Socket client, SimpleChatServer callback) throws IOException {
        SimpleChat.serverLogger.log(INFO, "Client tries to connect ... " + client);

        this.client = client;
        client.setKeepAlive(true);

        this.in = new BufferedReader(
                new InputStreamReader(
                        client.getInputStream(),
                        StandardCharsets.UTF_8));
        this.out = new PrintWriter(client.getOutputStream(), true);
        this.callback = callback;

        SimpleChat.serverLogger.log(INFO, "ClientWorker was initialized! clientSocket=" + client.toString()
                + " in=" + in.toString() + " out=" + out.toString());
    }

    ClientWorker(int id, MessageProducer producer, MessageConsumer consumer, Session session, Connection connection, SimpleChatServer callback) {
        this.id = id;
        this.producer = producer;
        this.consumer = consumer;
        this.session = session;
        this.connection = connection;
        this.callback = callback;
    }

    public int getId() {
        return id;
    }

    /**
     * MessageHandler for incoming Messages on Client Socket
     * <br>
     * The InputSocket will be read synchronous through readLine()
     * Incoming messages first will be checked if they start with any Commands, which will be executed properly.
     * Otherwise text messages will be delegated to the {@link SimpleChatServer#received(String, ClientWorker)} method.
     */
    @Override
    public void run() {
        String currentMessage;
        SimpleChat.serverLogger.log(INFO, "ClientWorker was started!");
        try {
            while ((currentMessage = ((TextMessage) consumer.receive()).getText()) != null && listening) {
            //while ((currentMessage = in.readLine()) != null && listening) {
                SimpleChat.serverLogger.log(INFO, "Received from client: " + currentMessage);

                if (currentMessage.startsWith("!")) {
                    try {
                        switch (MessageProtocol.getCommand(currentMessage.split(" ")[0])) {
                            case EXIT:
                                listening = false;
                                callback.removeClient(this);
                                break;
                            case CHATNAME:
                                String name = currentMessage.split(" ")[1];
                                callback.setName(name, this);
                                break;
                            case GROUP:
                                String[] tmp = currentMessage.split(" ");
                                for(int i = 4; i < tmp.length; i++) {
                                    tmp[3] += " " + tmp[i];
                                }
                                // Functions with param like !GROUP invite {Franz, Sissi, DarthVader}
                                if(tmp.length >= 4) {
                                    groupFunctions(tmp[1], tmp[2], tmp[3]);
                                }
                                // Funktions without like !GROUP leave
                                else groupFunctions(tmp[1], tmp[2], "");
                                break;
                            default:
                                // throw MalformedCommandException
                        }
                    } catch (IllegalArgumentException iae) {
                        SimpleChat.serverLogger.log(SEVERE, iae.toString());
                    }
                } else callback.received(currentMessage, this);
            }
        } catch (Exception e) {
            SimpleChat.serverLogger.log(SEVERE, e.toString());
        } finally {
            //if (!client.isClosed()) shutdown();
        }
    }

    /**
     * TODO group functions
     * @param command
     * @param param
     */
    public void groupFunctions(String groupName, String command, String param) {
        String name = "";
        String[] users;
        // note: message user send to every active group member current list of users in the group
        switch (command) {
            case "msg":         // Get normal messages
                callback.receivedGroup(param, this, groupName);
                break;
            case "create":      // Create group command
                name = callback.addGroup(groupName, this);
                send("Welcome to " + name , name);
                // Send user information for ListView
                send("user " + callback.getGroupClients(groupName), groupName);
                break;
            case "invite":      // Invite new user command
                param = param.replace("{", "").replace("}", "");
                users = param.split(", ");
                callback.send("invite " + groupName, groupName, users);
                break;
            case "ack":         // Acknowledgment of invite
                name = callback.getChatName(this);
                callback.addUserToGroup(groupName, name);
                callback.send("user " + callback.getGroupClients(groupName), groupName);
            case "kick":        // Kick user from group (if admin)
                param = param.replace("{", "").replace("}", "");
                users = param.split(", ");
                if(callback.kickUsersFromGroup(groupName, users, this)) {
                    callback.send("kick" , groupName, users);
                    callback.send("user " + callback.getGroupClients(groupName), groupName);
                }
                break;
            case "destroy":     // Destroy a group (if last admin)
                callback.destroyGroup(groupName, this);
                break;
            case "leave":       // Leave a group (works not if last admin)
                if(callback.leaveGroup(groupName, this)) {
                    users = new String[1];
                    users[0] = callback.getChatName(this);
                    callback.send("kick" , groupName, users);
                    callback.send("user " + callback.getGroupClients(groupName), groupName);
                }
                break;
            case "admin":       // Make a user a new group admin
                if(callback.addAdminToGroup(groupName, param, this)) {
                    callback.send("New Admin -> " + param , groupName);
                    callback.send("user " + callback.getGroupClients(groupName), groupName);
                }
                break;
        }
    }

    /**
     * Clean shutdown of ClientWorker
     * <br>
     * If listening was still true, we are sending a {@link MessageProtocol.Commands#EXIT} to the client.
     * Finally we are closing all open resources.
     */
    void shutdown() {
        SimpleChat.serverLogger.log(INFO, "Shutting down ClientWorker ... listening=" + listening);
        if (listening) {
            listening = false;
            send(MessageProtocol.getMessage(EXIT));
        }

        /**
        try {
            in.close();
            out.close();
            client.close();
        } catch (IOException io) {
            SimpleChat.serverLogger.log(SEVERE, io.toString());
        }*/
    }

    /**
     * TODO sending to all back - client is connected
     * Sending message through Socket OutputStream {@link #out}
     *
     * @param message MessageText for Client
     */
    void send(String message) {
        try {
            /*if (client.isConnected()) {
                SimpleChat.serverLogger.log(INFO, "Sending message to client: " + message);
                out.println(message);
                out.flush();
            }*/
            SimpleChat.serverLogger.log(INFO, "Sending message to client: " + message);
            TextMessage msg = session
                    .createTextMessage(message);
            producer.send(msg);
        } catch (Exception e) {
            SimpleChat.serverLogger.log(SEVERE, e.toString());
            shutdown();
        }
    }

    /**
     * TODO  ClientWorker sendBack
     * Sending message through Socket OutputStream {@link #out}
     *
     * @param message MessageText for Client
     */
    void send(String message, String groupName) {
        try {
            /*if (client.isConnected()) {
                SimpleChat.serverLogger.log(INFO, "Sending message to client: " + message);
                out.println(message);
                out.flush();
            }*/
            SimpleChat.serverLogger.log(INFO, "Sending message to group: " + message);
            TextMessage msg = session
                    .createTextMessage("!GROUP " + groupName + " " + message);
            producer.send(msg);
        } catch (Exception e) {
            SimpleChat.serverLogger.log(SEVERE, e.toString());
            shutdown();
        }
    }
}
