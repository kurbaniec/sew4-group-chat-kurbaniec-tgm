package simplechat.communication.socket.client;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.benchmark.Producer;
import simplechat.client.SimpleChat;
import simplechat.communication.MessageProtocol;

import javax.jms.*;
import javax.xml.soap.Text;

import static simplechat.communication.MessageProtocol.Commands.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.Random;

import static java.util.logging.Level.*;

/**
 * SimpleChatClient connects to SimpleChatServer with the choosen communication protocol and initiates a UI.
 * <br>
 * Default settings for the main attributes will be: name="Client" host="localhost" and port=5050
 */
public class SimpleChatClient extends Thread {

    private String name = "Client";
    private String host = "localhost";
    private Integer port = 61616;

    private InetSocketAddress socketAddress;
    private Socket socket = null;
    private PrintWriter out;
    private BufferedReader in;

    private boolean listening = false;
    private String currentMessage;
    private MessageProducer producer;
    private Destination clientDest;
    private Session session;
    private Connection connection;

    private SimpleChat client;

    /**
     * Initializes host, port and callback for UserInterface interactions.
     *
     * @param name   String representation of chatName
     * @param host   String representation of hostname, on which the server should listen
     * @param port   Integer for the listening port
     * @param client UserInterface callback reference for user interactions
     */
    public SimpleChatClient(String name, String host, Integer port, SimpleChat client) {
        if (name != null) this.name = name;
        if (host != null) this.host = host;
        if (port != null) this.port = port;
        this.client = client;
        SimpleChat.clientLogger.log(INFO, "Init: host=" + this.host + " port="
                + this.port + " chatName=" + this.name);
    }

    /**
     * Initiating the Socket with already defined Parameters (host, port). Also a timeout of 2000 ms is set at connect.
     * The {@link java.net.Socket#setKeepAlive(boolean)} is set to true.
     * <br>
     * After activating {@link #listening}, the Chatname will be sent to the Server and the reading loop is started,
     * checking for the {@link BufferedReader#readLine()} and the {@link #listening} flag.
     * <br>
     * In case of an Exception the Thread will be interrupted and if the socket was connected and bound,
     * the {@link #shutdown()} method will be called.
     */
    public void run() {
        try {
            ConnectionFactory connectionFactory =
                    new ActiveMQConnectionFactory("tcp://" + host + ":" + port);
            connection = connectionFactory.createConnection();
            connection.start();

            //Creating a non transactional session to send/receive JMS message.
            session = connection.createSession(false,
                    Session.AUTO_ACKNOWLEDGE);

            Destination serverDest = session.createQueue("server-registration");

            // MessageProducer is used for sending messages to the queue.
            MessageProducer producerBegin = session.createProducer(serverDest);

            Destination replyDest = session.createTemporaryQueue();

            // We will send a small text message saying 'Hello World!!!'
            TextMessage message = session
                    .createTextMessage(MessageProtocol.getMessage(CHATNAME) + " " + name);

            message.setJMSReplyTo(replyDest);
            message.setJMSCorrelationID(Long.toHexString(new Random(System.currentTimeMillis()).nextLong()));

            // Here we are sending our message!
            producerBegin.send(message);

            MessageConsumer consumer = session.createConsumer(replyDest);
            TextMessage reply = (TextMessage)consumer.receive();
            SimpleChat.clientLogger.info("RECEIVED ID: "+reply.getText());

            clientDest = session.createQueue("Client#" + reply.getText());
            consumer = session.createConsumer(clientDest);
            serverDest = session.createQueue("Server#" + reply.getText());
            producer = session.createProducer(serverDest);

            listening = true;
            while ((currentMessage = ((TextMessage) consumer.receive()).getText()) != null && listening) {
                SimpleChat.clientLogger.log(INFO, "Received from server: " + currentMessage);
                received();
            }
        } catch (Exception ex) {
            SimpleChat.clientLogger.warning(ex.getMessage());
            /**
            if (io.getMessage().equals("Connection refused (Connection refused)")) {
                this.interrupt();
            }
            if (io.getMessage().equals("Stream closed")) {
                this.interrupt();
            } else
                SimpleChat.clientLogger.log(SEVERE, io.toString());*/
        } finally {
            //if (socket.isBound()) shutdown();
            shutdown();
        }
    }

    /**
     * Analyzing received messages.
     * <br>
     * If Server sends proper {@link simplechat.communication.MessageProtocol.Commands} this method will act accordingly.
     * <br>
     * {@link simplechat.communication.MessageProtocol.Commands#EXIT} will set listening to false
     * and then calls {@link #shutdown()}
     * <br>
     * If there is now Command (no "!" as first character),
     * the message will be passed to {@link simplechat.client.SimpleChat#incomingMessage(String)}
     */
    private void received() {
        if (currentMessage.startsWith("!")) {
            try {
                switch (MessageProtocol.getCommand(currentMessage.split(" ")[0])) {
                    case EXIT:
                        listening = false;
                        shutdown();
                        client.incomingMessage("Server has closed the connection!");
                        break;
                    // Group-Chat Options
                    case GROUP:
                        String[] msg = currentMessage.split(" ");
                        switch (msg[2]) {
                            case "invite":
                                // Ask user if he wants to join the group
                                if(client.showInviteDialog(msg[3])) {
                                    // If yes, do it
                                    client.addChat(msg[3]);
                                    send("!GROUP ack", msg[3]);
                                }
                                // When not, send a message to the server, that you don´t
                                // want to join the group (because everyone is automatically added
                                // in the process)
                                else send("!GROUP leave", msg[3]);
                                break;
                            case "kick":
                                // !GROUP baum kick {Baum}
                                client.kick(msg[1]);
                                break;
                            case "user":
                                msg[3] = currentMessage.substring(currentMessage.indexOf(msg[3]));
                                client.setClients(msg[1], msg[3]);
                                break;
                            default:
                                // Case for default messages
                                if(msg.length >= 4) {
                                    // If message consist of more than one word, bind them together
                                    msg[2] = currentMessage.substring(currentMessage.indexOf(msg[2]));
                                }
                                // Inform gui
                                client.incomingMessage(msg[2], msg[1]);
                        }
                    default:
                }
            } catch (IllegalArgumentException iae) {
                SimpleChat.clientLogger.log(SEVERE, iae.toString());
            }
        } else {
            SimpleChat.clientLogger.log(INFO, "Passing message to client UI ...");
            client.incomingMessage(currentMessage);
        }
    }

    /**
     * Sending message to the server through network
     *
     * @param message Public message for server intercommunication
     */
    public void send(String message) {
        //TODO implement private messaging
        try {
            if(producer != null) {
            //if (socket.isConnected()) {
                SimpleChat.clientLogger.log(INFO, "Sending message to server: " + message);
                /**
                out.println(message);
                out.flush();*/
                if(message.contains("!PRIVATE")) {
                    String[] privat = message.split(" ");
                    TextMessage msg = session
                            .createTextMessage("!GROUP create " + name + "-" + privat[1]);
                    producer.send(msg);
                    msg = session
                            .createTextMessage("!GROUP invite " + "{" + privat[1] + "}");
                    producer.send(msg);
                    msg = session
                            .createTextMessage("!GROUP admin " + privat[1]);
                    producer.send(msg);
                }
                else {
                    TextMessage msg = session
                            .createTextMessage(message);
                    producer.send(msg);
                }
            }
        } catch (Exception e) {
            SimpleChat.clientLogger.log(SEVERE, e.toString());
            if (!socket.isClosed()) shutdown();
        }
    }

    /**
     * Sending message to the server through network for private Message
     *
     * @param message  Private message for client-to-client intercommunication
     * @param groupName Name of receiver
     */
    public void send(String message, String groupName) {
        //TODO implement private messaging
        try {
            if(producer != null) {
                SimpleChat.clientLogger.log(INFO, "Sending message to server: " + message +
                        " | Destination-Chat: " + groupName);
                if(!message.contains("!GROUP")) {
                    TextMessage msg = session
                            .createTextMessage("!GROUP " + groupName + " msg " + message);
                    producer.send(msg);
                }
                else {
                    // Convert to !GROUP standard
                    message = "!GROUP " + groupName + message.substring(message.indexOf("!GROUP")+6);
                    TextMessage msg = session
                            .createTextMessage(message);
                    producer.send(msg);
                }
            }
        } catch (Exception e) {
            SimpleChat.clientLogger.log(SEVERE, e.toString());
            if (!socket.isClosed()) shutdown();
        }
    }

    /**
     * Clean shutdown of Client
     * <br>
     * If listening was still true, we are sending a {@link MessageProtocol.Commands#EXIT} to the server.
     * Finally we are closing all open resources.
     */
    public void shutdown() {
        SimpleChat.clientLogger.log(INFO, "Shutting down Client ... listening=" + listening);
        if (listening) {
            listening = false;
            send(MessageProtocol.getMessage(EXIT));
            client.incomingMessage("Closed connection to server!");
            try {
                session.close();
                connection.close();
            }
            catch (Exception ex) {
                SimpleChat.clientLogger.warning("Problem in stopping client: " + ex.getMessage());
            }

        }

        /**
        if (socket.isConnected())
            try {
                in.close();
                out.close();
                socket.close();
            } catch (Exception e) {
                SimpleChat.clientLogger.log(SEVERE, e.toString());
            } finally {
                this.interrupt();
            }
         */
    }

    /**
     * @return True if still listening and online
     */
    public boolean isListening() {
        return listening;
    }
}
