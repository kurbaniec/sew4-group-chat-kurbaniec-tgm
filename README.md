# "Java GUI & Socket Programmierung - Group Chat"

## Aufgabenstellung
Die detaillierte [Aufgabenstellung](TASK.md) beschreibt die notwendigen Schritte zur Realisierung.

## Vorraussetzungen

* ApacheMQ installiert und am Laufen   
  Um das Programm auszuführen, muss ApacheMQ am Server ausgeführt werden. Das Programm ist [hier](https://activemq.apache.org/) zu finden. Man entpackt dieses einfach und startet es via `.\bin\activemq start`.

## Implementierung

### ApacheMQ

Zu aller erst wurden die fehlenden `client.fxml ` und `server.fxml `, damit eine GUIs aus den Elementen erstellt werden kann. 

Der erste Schritt war die Implementierung einer Message-Oriented-Middleware. Gewählt habe ich ApacheMQ, da damit schon im Unterricht gearbeitet wurde.

Bei dieser Art der Verbindung wird zuerst eine Connection und Session für die MQ ersellt:

```java
ConnectionFactory connectionFactory =
                    new ActiveMQConnectionFactory("tcp://" + host + ":" + port);
connection = connectionFactory.createConnection();
connection.start();

session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
```

Danach wird eine eine Destination erstellt, dies ist die Ziel-Ort-Queue, als an welche Queue gesendet werden soll. Aus dieser wird ein MessageProducer erstellt, um Nachrichten senden zu können.

```java
Destination serverDest = session.createQueue("server-registration");
producer = session.createProducer(serverDest);
```

Über den Producer kann dann eine Nachricht, wie eine einfache TextMessage, versendet werden:

```
TextMessage msg = session.createTextMessage(message);
producer.send(msg);
```

Analog wird zum Empfangen ein Consumer benötigt, über den Nachrichten geholt werden können.

```
MessageConsumer consumer = session.createConsumer(destination);
currentMessage = ((TextMessage) consumer.receive()).getText());s
```

### GUI:

Bei der GUI habe ich beim Client eine zusätzliche VBox (chatPanel), bei der Buttons die einzelnen Chats repräsentieren.

```java
public void addChat(String groupName) {
    if(!chatHistory.containsKey(groupName)) {
        chatHistory.put(groupName, "Welcome to Group Chat");
        Button newGroup = new Button(groupName);
        newGroup.setOnAction(this::changeChat);
        newGroup.setMaxWidth(Double.MAX_VALUE);
        Platform.runLater(() -> chatPanel.getChildren().add(newGroup));
    }
    activeChat = groupName;
}
```

Falls neue Nachrichten ankommen, werden diese in einer HashMap gespeichert, wo der Key der Gruppen-Name ist und der Inhalt die Nachrichten, formatiert in einem String, sind. 

```java
	public void updateTextAreaWithText(String text, String groupName) {
        chatHistory.put(groupName, chatHistory.get(groupName) + "\n" + text);
        if(activeChat.equals(groupName)) {
            textArea.setText(chatHistory.get(groupName));
            textArea.appendText("");
        }
    }
```

Falls man den Chat wechselt, müssen die korrekten Daten im Textfeld angezeigt werden, die werden aus der vorher erwähnten HashMap geholt.

```java
	@FXML
    protected void changeChat(ActionEvent event) {
        String chatName = ((Button)event.getSource()).getText();
        if (chatHistory.containsKey(chatName)) {
            activeChat = chatName;
            String history = chatHistory.get(chatName);
            textArea.setText(history);
            textArea.appendText("");
        }
    }
```

## Logik

Im allgemeinen Chat können Befehle wie *!CHATNAME* und co. benutzt werden.

Wichtigste Neuerung ist, dass man mittels *!GROUP create PrivateGruppe* neue Gruppen erstellen kann. Über diese findet ein "geheimer" Nachrichtentausch, der nur von ausgewählten Mitgliedern eingesehen werden.

Folgende Befehle können **nur** in Gruppen benutzt werden:

- *!GROUP invite {Franz, Sissi, DarthVader}*
- *!GROUP kick {Sissi, DarthVader}*
- *!GROUP destroy*
- *!GROUP accept PrivateGrupp* (Durch GUI und nicht Kommando-Eingabe gelöst)
- *!GROUP leave*
- *!GROUP admin Franz*

Intern werden die Befehle in *!GROUP [gruppenname] [kommando] [parameter*]  umgewandelt, also es wird immer der Gruppenname mitgeschickt. Damit kann der Server leichter Gruppen erkennen und verschiedene Operation in Bezug auf diese ausführen.

Falls eine Nachricht von einer Gruppe (*!GROUP [gruppenname] msg [nachricht]*) gesendet wird, sendet der Server ähnlich wie in eine einem Observer-Pattern zu allen registrierten Nutzern der Gruppe eine Nachricht. 

*Hinweis:* Index 1 wird nicht verwendet, in diesem werden alle Admins gespeichert.

```java
/**
     * Sending messages to a group.
     * Resembles kind of the observer pattern.
     *
     * @param message  MessageText with sender ChatName
     * @param groupName target group
     */
     public void send(String message, String groupName) {
         try {
             ArrayList<String> group = server.getGroup(groupName);
             for(int i = 1; i < group.size(); i++) {
                 getWorker((Object)group.get(i)).send(message, groupName);
             }
         } catch (Exception e) {
            SimpleChat.serverLogger.log(SEVERE, e.toString());
         }
     }
```



Mittlerweile wurde auch eine Gruppensicht eingeführt, wo alle Clients aufgelistet werden. Ändert ein Client seinen Name oder wird zum Admin hochgestuft, dann wird dies auch grafisch angezeigt.

![beta](img/showcase.PNG)



## Abschließender Kommentar

Aus Zeitgründen wurde manche Tasks nicht erfüllt bzw. nicht vollständig erfüllt. Mein Group-Chat erfüllt im grundlegenden die meisten Bedingungen, ich würde ihn aber nicht als sauber implementiert bzw. fehlerlos ansehen. Im folgenden möchte ich kurz erläutern, was mich am momentanen Stand des Programmes stört und wie man diese Probleme vielleicht beheben könnte. 

* Teilweise finden sich noch Relikte von der Socket-Implementierung im Code, man müsste diese langsam entfernen und schauen ob sie doch im Hintergrund nicht was am Programm grundlegend verändern und die Lauffähigkeit behindern. 
* Die MoM-Implementierung. Derzeit schaut das Programm nicht ob diese läuft/funktionstüchtig ist. Ich habe auf die schnellen keine Lösungen dafür gefunden, aber es muss sicherlich irgendwie gehen. Außerdem sollten die Queues programmatisch nach dem Beenden einer Verbindung gelöscht werden, weil teilweise veraltete Nachricht beim Neustart zu Problemen führen.
* Observer? Das Benachrichtigen der User einer Gruppe sollte via Observer implementiert werden und nicht mit dem derzeitigen Pseudo-Observer. Das Senden erfolgt teilweise nach dem Prinzip, via einer Schleife, die als Art update fungiert, werden die User einer Gruppen-Liste benachrichtigt. Das Adden/Removen dagegen nicht wirklich.
* Die ganzen Messages und Kommandos, die leider noch statisch auf Strings basieren werden langsam untragbar zum Implementieren/Verändern. Ich habe mir das Command-Pattern angeschaut, das erfüllt irgendwie nicht die Bedingungen, da Gruppen keine Objekte per se sind. Ich glaube es wäre am sinnvollsten ein eigenes Objekt zur Datenübertragung zu verwenden, was mit einer Queue problemlos funktioniert, was die einzelnen Kommandos, Parameter, etc. einfach zur weiteren Verarbeitung am Server, aber auch am Client ermöglichen sollte. Die derzeitige TextMessage-String-Lösung ist wirklich alles andere als elegant und führt zu unnötigen split-Befehlen, substrings und mehr. Allgemein sehr viel Boiler-Code.
* Was noch an Funktonen im Programm fehlt, ist das Benutzen allgemeiner Kommandos (*!CHATNAME,* etc.) in Gruppenchats. Dies sollte durch irgendwelche Umwandlungen beim Checken möglich sein. Außerdem fehlt der *!PRIVATE user* Befehl, dieser sollte einfach meiner Meinung am Client intern einfach durch das *!GROUP create myuser-otheruser* und *!GROUP invite otheruser*  Kommando ersetzt werden.

## Quellen

* [ApacheMQ - Queue Request - Response; 2019.06.10](https://michaelrice.com/2015/05/the-simplest-jms-requestreply-i-could-come-up-with-for-activemqjboss-a-mq/)
* [Destroy Queue; 2019.06.10](https://self-learning-java-tutorial.blogspot.com/2018/08/activemq-delete-queue-programmatically.html) 
* [Get Button-Text, that triggers event - JavaFX; 2019.06.11](https://stackoverflow.com/questions/35676519/actionevent-get-source-of-button-javafx)
* [Add elements to VBox - JavaFX; 2019.06.11](https://stackoverflow.com/questions/29247131/how-do-i-add-components-from-top-to-bottom-in-vbox)
* [JavaFX-Dialog; 2019.06.12](https://code.makery.ch/blog/javafx-dialogs-official/)
* [JavaFX-Dialog - Auf Ergebnis warten + Timer; 2019.06.12](https://stackoverflow.com/questions/13796595/return-result-from-javafx-platform-runlater)
